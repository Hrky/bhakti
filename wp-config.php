<?php
/**
 * Temeljna konfiguracija WordPressa.
 *
 * wp-config.php instalacijska skripta koristi ovaj zapis tijekom instalacije.
 * Ne morate koristiti web stranicu, samo kopirajte i preimenujte ovaj zapis
 * u "wp-config.php" datoteku i popunite tražene vrijednosti.
 *
 * Ovaj zapis sadrži sljedeće konfiguracije:
 *
 * * MySQL postavke
 * * Tajne ključeve
 * * Prefiks tablica baze podataka
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL postavke - Informacije možete dobiti od vašeg web hosta ** //
/** Ime baze podataka za WordPress */
define( 'DB_NAME', 'bhakti' );

/** MySQL korisničko ime baze podataka */
define( 'DB_USER', 'root' );

/** MySQL lozinka baze podataka */
define( 'DB_PASSWORD', 'm11' );

/** MySQL naziv hosta */
define( 'DB_HOST', 'localhost' );

/** Kodna tablica koja će se koristiti u kreiranju tablica baze podataka. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Tip sortiranja (collate) baze podataka. Ne mijenjate ako ne znate što radite. */
define('DB_COLLATE', '');
define('FS_METHOD', 'direct');
/**#@+
 * Jedinstveni Autentifikacijski ključevi (Authentication Unique Keys and Salts).
 *
 * Promijenite ovo u vaše jedinstvene fraze!
 * Ključeve možete generirati pomoću {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org servis tajnih-ključeva}
 * Ključeve možete promijeniti bilo kada s tim da će se svi korisnici morati ponovo prijaviti jer kolačići (cookies) neće više važiti nakon izmjene ključeva.
 *
 * @od inačice 2.6.0
 */
define( 'AUTH_KEY',         'VhGfG4>!l&}S-.K|SK.vv&+dIM7f(mR^>_ ,esDiEl yN]0>hhzYplZ)q&I@eH6T' );
define( 'SECURE_AUTH_KEY',  'J%!2P7kj<E.:!o@&]ExGFbnykKR0)-)=(rUB8($6}+k<i)`E0{*s&iV,tTIn2z>b' );
define( 'LOGGED_IN_KEY',    'wi~wW/HQe _L=YrY^$*p[>!g0}>hKV^CG2=<+95z5U|Z*cy1CQL$FXi`q,5d0n(f' );
define( 'NONCE_KEY',        '7eQYjRHb0#eaK))(M_A8pZ$A65q/Q(1-lf^1yHps1-i2:Q2Gs&]k(hw{IN}}:Sy~' );
define( 'AUTH_SALT',        '[v4^|Ht?;OHz~2d-<C!i~oQ PXbV&(joZxYA$rMV|/Z,a_T68dsg]nSH0]/jBwa ' );
define( 'SECURE_AUTH_SALT', 'ST.$EKy#Emz<TOl:KjD)r9dH%g=ln9%xtAIi>mVoyPiw2cJ}O@@}7?0Sa<!a_X_Z' );
define( 'LOGGED_IN_SALT',   'h: oS= 0j4~)^j%]a0+Ql?C`<1)73d1Pc4`2 xs=HU_N$N~av`$IZq_V*TZdk&lP' );
define( 'NONCE_SALT',       '`O<Y,<T;0Riue=Q_$)jLgASfYDJF$IYlI(g=Bz:A*7kn8Rk>QH#(X]%zQO1`|#ad' );

/**#@-*/

/**
 * Prefix WordPress tablica baze podataka.
 *
 * Možete imati više instalacija unutar jedne baze podataka ukoliko svakoj dodjelite
 * jedinstveni prefiks. Koristite samo brojeve, slova, i donju crticu!
 */
$table_prefix  = 'wp_';

/**
 * Za programere: WordPress debugging mode.
 *
 * Promijenit ovo u true kako bi omogućili prikazivanje poruka tijekom razvoja.
 * Izrazito preporučujemo da programeri dodataka (plugin) i tema
 * koriste WP_DEBUG u njihovom razvojnom okružju.
 *
 * Za informacije o drugim konstantama koje se mogu koristiti za debugging,
 * posjetite Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To je sve, ne morate više ništa mijenjati! Sretno bloganje. */

/** Apsolutna putanja do WordPress mape. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Postavke za WordPress varijable i već uključene zapise. */
require_once(ABSPATH . 'wp-settings.php');
