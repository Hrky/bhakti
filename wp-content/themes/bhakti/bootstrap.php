<?php

ini_set('date.timezone', 'Europe/Zagreb');

require_once(get_theme_root() . '/hopsin/breadcrumb.class.php');


/**
 * Hopsin Kernell
 */
class Hopsin {


	static function buildBreadcrumb($menu) {
		return new Breadcrumbs($menu);
	}

	static function upgradeQueryString($link, $queryStr) {
		
		$urlArr = parse_url($link);

		$queryArr = array();
		if(!empty($urlArr['query']))
		{
			parse_str($urlArr['query'], $queryArr);
		}

		parse_str($queryStr, $linkArr);
		
		$queryStr = http_build_query(array_merge($queryArr, $linkArr));
		
		$ex = explode("?", $link);
		
		$queryStr = !empty($queryStr) ? "?". $queryStr : "";

		return $ex[0] . $queryStr;
	}
}
