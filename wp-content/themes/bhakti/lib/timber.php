<?php
use Hopsin\Setup;

/**
 * Check if Timber is activated
 */

if ( ! class_exists( 'Timber' ) )
{
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    } );
    return;
}

/**
 * Timber
 */
class Hopsin extends TimberSite {

    function __construct() {
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        parent::__construct();
    }


    function add_to_context( $context ) {


		

        /* Menu */
        $context['menu_main'] = new TimberMenu('menu_main');
        $context['menu_footer'] = new TimberMenu('menu_footer');

        /* Breadcrumb */
		$context['menu_breadcrumb'] = Hopsin::buildBreadcrumb($context['menu_main']);
		if(!empty($context['menu_breadcrumb']))
		{
			$context['menu_breadcrumb_parent'] = $context['menu_breadcrumb']->getParentPost();
		}
		if(!empty($context['menu_breadcrumb_html']))
		{
			$context['menu_breadcrumb_html'] = $context['menu_breadcrumb']->getHtml();
		}
			//$context['menu_breadcrumb_posts'] = $context['menu_breadcrumb']->getPosts();
	
		/* Site info */
        $context['site'] = $this;
        $context['admin_ajax_url'] = admin_url('admin-ajax.php');

        /* Site info */
        $context['display_sidebar'] = Setup\display_sidebar();

        $context['sidebar_primary'] = Timber::get_widgets('sidebar-primary');

        $context['theme_url'] = get_template_directory_uri();

        $uri = preg_replace('/[A-Z]:/', '', get_theme_root(), 1);

		// $context['wpml_current_lang'] = ICL_LANGUAGE_CODE;


        return $context;
    }
}
new Hopsin();
