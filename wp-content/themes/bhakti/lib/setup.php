<?php

namespace Hopsin\Setup;

use Hopsin\Assets;

/**
 * Theme setup
 */
function setup() {

  //
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  //add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('nivas', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');


  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  //add_editor_style(Assets\asset_path('styles/css/app.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

// /**
//  * Register sidebars
//  */
// function widgets_init() {
//   register_sidebar([
//     'name'          => __('Primary', 'nivas'),
//     'id'            => 'sidebar-primary',
//     'before_widget' => '<section class="widget %1$s %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3>',
//     'after_title'   => '</h3>'
//   ]);

//   register_sidebar([
//     'name'          => __('Footer', 'nivas'),
//     'id'            => 'sidebar-footer',
//     'before_widget' => '<section class="widget %1$s %2$s">',
//     'after_widget'  => '</section>',
//     'before_title'  => '<h3>',
//     'after_title'   => '</h3>'
//   ]);
// }
// add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {

    // wp_enqueue_style('css', '/wp-content/themes/hopsin/assets/styles/scss/lib/preloader.css', false, null);
    // wp_enqueue_style('css', '/wp-content/themes/hopsin/assets/styles/scss/lib/preloader.css', false, null);
	// wp_enqueue_style('css', '/wp-content/themes/hopsin/assets/styles/scss/lib/bootstrap.css', false, null);
    // wp_enqueue_style('css', '/wp-content/themes/hopsin/assets/styles/scss/lib/animate.css', false, null);
    // wp_enqueue_style('css', '/wp-content/themes/hopsin/assets/styles/scss/lib/revolution.css', false, null);
	
	
	wp_enqueue_style('css', Assets\asset_path('styles/css/app.css'), false, null);

    // comment scripts --> show this file only on the single page
    if (is_single() && comments_open() && get_option('thread_comments'))
    {
        wp_enqueue_script('comment-reply');
    }

    // head js
    wp_enqueue_script('app-head', Assets\asset_path('scripts/app-head.js'), null, false);

    // footer js
    wp_enqueue_script('app-footer', Assets\asset_path('scripts/app-footer.js'), null, false, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);



// 