<?php
 /**
     * Post Type: PO.
     */

    $labels = array(
        "name" => __( "Music", "" ),
        "singular_name" => __( "Music", "" ),
    );

    $args = array(
        "label" => __( "Music", "" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => true,
        "rewrite" => array( "slug" => "music", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "author", "thumbnail", "page-attributes" ),
    );

    register_post_type( "music", $args );