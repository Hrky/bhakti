<?php
require_once( ABSPATH . 'wp-admin/includes/media.php' );
$wp_includes = [
    'routes.php',           // Router
    'lib/timber.php',       // Twig
    'lib/assets.php',       // Scripts and Stylesheets
    // 'lib/extras.php',       // Custom Functions
    'lib/setup.php',        // Theme Setup
    'lib/titles.php',       // Page Titles
    'lib/cpt.php',
];

foreach ($wp_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'nivas_wp'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);

add_filter('timber/twig', 'timber_twig');

function timber_twig($twig)
{
    $twig->addExtension(new Twig_Extension_StringLoader());
    $twig->addFilter(new Twig_SimpleFilter('print_r', 'twig_print_r'));
    $twig->addFunction(new Twig_SimpleFunction('generate_friendly_name', 'generate_friendly_name'));
    $twig->addFilter(new Twig_SimpleFilter('get_image_by_id', 'get_image_by_id'));
    $twig->addFilter(new Twig_SimpleFilter('get_attachment_image_by_id', 'get_attachment_image_by_id'));
    $twig->addFunction(new Twig_SimpleFunction('get_music_by_shortcode', 'get_music_by_shortcode'));

    return $twig;
}

function twig_print_r($row)
{
    print_r($row);
}

function get_image_by_id($id)
{
	return wp_get_attachment_url($id);
}

function generate_friendly_name($value)
{
	$value = sanitize_title(strval($value));

	return $value;
}

function get_attachment_image_by_id($id)
{
	$value = wp_get_attachment_image($id);

	return $value;
}
// wp_get_attachment_image

function get_music_by_shortcode($playlist_shortcode = '[playlist ids="47]')
{
	// $playlist_shortcode = '[playlist ids="47]';

	// Find registered tag names in your $playlist_shortcode.
	preg_match('/' . get_shortcode_regex() . '/', $playlist_shortcode, $match );

	// Parse playlist shortcode attributes
	$playlist_attr = shortcode_parse_atts($match[3]);
	
	$audios = array();

	// Retrieve audio ids
	$audio_id = explode(',',$playlist_attr['ids']);
	foreach($audio_id as $key => $id ){
		
		$file_path = get_attached_file($id);

		$audios[$key]["id"] = $id;
		$audios[$key]["name"] = get_the_title($id);
		$audios[$key]["url"] = wp_get_attachment_url($id);
		$audios[$key]["path"] = $file_path;
		$audios[$key]["data"] = get_post($id);
		// $audios[$key]["metadata"] = wp_read_audio_metadata($file_path);
		$audios[$key]["metadata"] = wp_get_attachment_metadata($id);
		// echo do_shortcode('[audio mp3=' . wp_get_attachment_url($id) . ']');
	}

	return $audios;
}