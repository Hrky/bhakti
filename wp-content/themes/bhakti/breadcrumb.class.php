<?php

class Breadcrumbs {

    private $posts; 
    private $request;

    function __construct($menu) {

        $this->request_uri = explode("/", $_SERVER['REQUEST_URI']);

        $this->walk($menu->items);
    }
    
    private function walk($items) {

		foreach($items as $item) {
            
			if ($item->current_item_ancestor || $item->current) {
                
                $this->posts[] = $item;
                
                if(!empty($item->children))
                {
                    $this->walk($item->children);
                }
            }
		}
    }
    
    public function getParentPost() {

        $parent_index = false;
		
		if ($this->posts == null)
        {
        	$total = 1;
        }
        else
        {
        	$total = count($this->posts);        	
        }
        
        if($total == 1)
        {   
            return array('path'=> get_home_url(), "title" => __('Naslovnica','themes') );
        }
        else if($total > 1)
        {
            $parent_index = $total - 2;
        }
        
   

        $parent_item = !empty($this->posts[$parent_index]) ? $this->posts[$parent_index] : false;
        if($parent_item === false && count($this->request_uri)>1)
        {
            // Fallback

            // check request uri
            $new_request_uri = $this->request_uri;

            // up
            array_pop($new_request_uri);

            // up
            array_pop($new_request_uri);
            
            $post_id =  url_to_postid( implode('/', $new_request_uri) );

            $post_query = array(
                'post_type' => 'any',
                'post__in' => array($post_id)
            );

            $posts = Timber::get_posts($post_query);
            if( !empty($posts) )
            {
                $parent_item = $posts[0];
            }

        }

        return $parent_item;
    }

    public function getPosts() {
        return $this->posts;
    }

    public function getHtml() {
        
        $str = "";

        $last_index = 0;

        if ($this->posts == null)
        {
        	$total = 1;
        }
        else
        {
        	$total = count($this->posts);        	
        }
       
        if($total > 0)
        {
            $last_index = $total - 1;
        }
        
        if($this->posts)
        {
            foreach ($this->posts as $key=>$value) {
    
                if($last_index == $key)
                {
                    $str .= "<span>".$value->title."</span>";
                }
                else
                {
                    $str .= '<a href="'. $value->url .'" title="'. $value->title .'">'. $value->title .'</a>\n';
                }
            }
        }

        return $str;
    }
}  