<?php
$context = Timber::get_context();


$args = array(
	'cat' => 2,
	'order_by' => 'date',
	'order' => 'DESC'
);

$context['news'] = Timber::get_posts($args);

Timber::render('templates/news.twig', $context);