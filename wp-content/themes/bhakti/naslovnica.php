<?php
/* Template Name: Naslovnica **/
$context = Timber::get_context();

$context['post'] = new Timber\Post();


$args = array(
	'cat' => 2,
	"limit" => 4,
	'order_by' => 'date',
	'order' => 'DESC'
);

$context['news'] = Timber::get_posts($args);

// print_r($context['post']);
// die();

Timber::render('templates/naslovnica.twig', $context);