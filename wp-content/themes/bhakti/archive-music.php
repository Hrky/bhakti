<?php
$context = Timber::get_context();

$args = array(
	'post_type' => 'music'
);

$context['albums'] = Timber::get_posts($args);

// print_r($context['post']);
// die();

Timber::render('templates/music.twig', $context);